/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QTextEdit *textEdit;
    QPushButton *setTailNumber;
    QLabel *delay_report_header;
    QLabel *label_2;
    QLabel *real_tail_number;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *label_5;
    QTextEdit *time_add;
    QPushButton *pushButton_2;
    QLabel *label_6;
    QTextEdit *event_add;
    QPushButton *pushButton;
    QTableView *events_table;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        textEdit = new QTextEdit(centralwidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(0, 80, 201, 31));
        setTailNumber = new QPushButton(centralwidget);
        setTailNumber->setObjectName(QString::fromUtf8("setTailNumber"));
        setTailNumber->setGeometry(QRect(210, 80, 111, 34));
        delay_report_header = new QLabel(centralwidget);
        delay_report_header->setObjectName(QString::fromUtf8("delay_report_header"));
        delay_report_header->setGeometry(QRect(10, 10, 210, 25));
        delay_report_header->setMinimumSize(QSize(20, 25));
        QFont font;
        font.setPointSize(18);
        font.setBold(true);
        font.setWeight(75);
        delay_report_header->setFont(font);
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 50, 120, 18));
        QFont font1;
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setWeight(75);
        label_2->setFont(font1);
        real_tail_number = new QLabel(centralwidget);
        real_tail_number->setObjectName(QString::fromUtf8("real_tail_number"));
        real_tail_number->setGeometry(QRect(140, 50, 80, 18));
        QFont font2;
        font2.setPointSize(14);
        real_tail_number->setFont(font2);
        horizontalLayoutWidget = new QWidget(centralwidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(0, 130, 671, 45));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(10, 10, 0, 0);
        label_5 = new QLabel(horizontalLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout->addWidget(label_5);

        time_add = new QTextEdit(horizontalLayoutWidget);
        time_add->setObjectName(QString::fromUtf8("time_add"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(30);
        sizePolicy.setVerticalStretch(30);
        sizePolicy.setHeightForWidth(time_add->sizePolicy().hasHeightForWidth());
        time_add->setSizePolicy(sizePolicy);
        time_add->setMaximumSize(QSize(100, 30));
        time_add->setBaseSize(QSize(30, 30));

        horizontalLayout->addWidget(time_add);

        pushButton_2 = new QPushButton(horizontalLayoutWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);

        label_6 = new QLabel(horizontalLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout->addWidget(label_6);

        event_add = new QTextEdit(horizontalLayoutWidget);
        event_add->setObjectName(QString::fromUtf8("event_add"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(event_add->sizePolicy().hasHeightForWidth());
        event_add->setSizePolicy(sizePolicy1);
        event_add->setMaximumSize(QSize(16777215, 30));

        horizontalLayout->addWidget(event_add);

        pushButton = new QPushButton(horizontalLayoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout->addWidget(pushButton);

        horizontalLayout->setStretch(1, 15);
        horizontalLayout->setStretch(2, 10);
        horizontalLayout->setStretch(4, 30);
        events_table = new QTableView(centralwidget);
        events_table->setObjectName(QString::fromUtf8("events_table"));
        events_table->setGeometry(QRect(10, 180, 781, 192));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 30));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        setTailNumber->setText(QCoreApplication::translate("MainWindow", "Set Tail Number", nullptr));
        delay_report_header->setText(QCoreApplication::translate("MainWindow", "New Delay Report", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Tail Number:", nullptr));
        real_tail_number->setText(QString());
        label_5->setText(QCoreApplication::translate("MainWindow", "Time: ", nullptr));
        pushButton_2->setText(QCoreApplication::translate("MainWindow", "Now", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "Event: ", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindow", "Add", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
